import express from 'express';

const app = express();
const port = 3000;

export const main = () => {
    app.use('/', express.static('dist'));

    app.listen(
        port,
        () => console.log(`Listening on port ${port}!`)
    );
};
