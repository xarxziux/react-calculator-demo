const invalidChars = /[&/<>"']/g;

const stripInvalidChars = str => str.replace(invalidChars, "");

export const sanitiseName = name => stripInvalidChars(name);

const limitValue = limit => val =>
      val > limit
      ? limit
      : val;

const limitAge = age => limitValue(150);

export const sanitiseAge = age => limitAge(Math.round(/^\d*/.exec(age)[0] - 0));

export const sanitiseBalance = bal => /^\d*(?:\.\d\d?)?/.exec(bal)[0] - 0;

export const sanitiseEmail = email => stripInvalidChars(email);

export const sanitiseAddress = address => stripInvalidChars(address);
