import React, {useState} from "react";

export const form = state => {
    const [calcStr, setCalcString] = useState("");

    const submitString = event => {
        event.preventDefault();
        state.callCalculator(calcStr);
    };

    return <form onSubmit={submitString}>
        <label>
            <input
                type="text"
                value={state.inputText}
                onChange={x => setCalcString(x.target.value)}/>
        </label>
        <input type="submit" value="Submit" />
    </form>;
};
