import React from "react";

export const text = (text, style) =>
    <div style={style}>{text}</div>;
