export const text1 = {
    color: "blue"
};

export const text2 = {
    color: "orange"
};

export const container1 = {
    borderStyle: "solid",
    borderWidth: "2px",
    borderColor: "cornsilk",
    margin: "10px",
    padding: "5px"
};

export const container2 = {
    borderStyle: "solid",
    borderWidth: "2px",
    borderColor: "blue",
    margin: "10px",
    padding: "5px"
};

export const container3 = {
    borderStyle: "solid",
    borderWidth: "2px",
    borderColor: "green",
    margin: "10px",
    padding: "5px"
};

export const form1 = {
    color: "red"
};
