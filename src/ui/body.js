import React from "react";

import {container} from "./components/container.js";
import * as css from "./css/css.js";
import {text} from "./components/text.js";
import {form} from "./components/form.js";

const getInitMessage = state => (
    state.initialMessage == null
        ? "No initMessage available"
        : state.initialMessage
);

export const body = (state) =>
    <div>
        {container(
            text(state.initialMessage, css.text1),
            css.container1
        )}
        {container(
            form(state, css.form1),
            css.container2
        )}
        {container(
            text(state.serverText, css.text2),
            css.container3
        )}
    </div>;
