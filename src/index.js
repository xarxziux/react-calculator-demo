import React from "react";
import ReactDOM from "react-dom";

import {body} from "./ui/body.js";
import {initialiseIO} from "./io/io.js";

// This function calls the main UI entry point, thereby re-rendering the React
// interface.  It gets passed to the state module allowing it to called from
// there whenever the state gets updated.
//const redrawUI = state => body(state);
//const body = initialiseBody(callCalculator);
// To start the interface, we pass the redrawUI function to the state object
// which registers it for future updates and responds with the initial state
// of the app.  We then pass that initial state to the UI root component.
const Main = () => initialiseIO(body);

ReactDOM.render(<Main />, document.getElementById("root"));
