import {settings} from "../config/settings.js";
import {initialiseState, updateState} from "./state.js";

const logError = text => console.log(text);
const callCalculator = input => {
    fetch(settings.calcEndpoint + input)
        .then(res => {
            return res.text();
        })
        .then(text => {
            console.log("Server response: " + text);
            updateState({"serverText": text});
        });
};

export const initialiseIO = redrawUI => (
    initialiseState(Object.assign({}, settings, {
        updateState,
        callCalculator,
        redrawUI,
        logError
    }))
);
