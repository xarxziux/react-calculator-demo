let state = {};

export const initialiseState = (initState) => {
    state = initState;
    return state.redrawUI(state);
};

export const updateState = (newState) => (
    new Promise((resolve, reject) => {
        state = Object.assign({}, state, newState);
        resolve(null);
    })
        .then(() => state.redrawUI(state))
);
