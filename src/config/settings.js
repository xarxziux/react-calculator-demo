export const settings = {
    //calcEndpoint: "https://spring-calculator-demo.herokuapp.com/api/calculator/",
    calcEndpoint: "http://localhost:5000/api/calculator/",
    initialMessage: "Enter a correctly-formatted calculation string (ex: \"2+3\")",
    serverText: "Enter a calculation"
};
